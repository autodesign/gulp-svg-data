import path from 'path';
import through from 'through2';
import SVGSpriter from 'svg-sprite';
import gutil from 'gulp-util';
import vinylFile from 'vinyl-file';

const pluginError = t => new gutil.PluginError('gulp-svg-data', t);

const maxDate = (...t) => new Date(Math.max(...t));

const splitFilename = (f) => {
  // car => [car]
  // car.hover => [car, hover]
  // car.smth.hover => [car.smth, hover]
  const parts = f.split('.');

  return parts.length === 1
    ? parts
    : [parts.slice(0, -1).join('.'), parts.slice(-1)];
};

const withValidFile = (file, cb, then) => {
  if (file.isNull()) {
    cb();
    return;
  }

  if (file.isStream()) {
    cb(pluginError('Streaming not supported')); // а надо бы
    return;
  }

  then(file);
};


const createTemplateFile = (templateFile, createName, files, data = null) => {
  const dirname = path.dirname(templateFile.path);
  const extname = path.extname(templateFile.path);
  const basename = path.basename(templateFile.path, extname);

  const newPath = path.join(dirname, createName(basename, extname));

  // берет за основу templateFile
  const newFile = templateFile.clone();

  // .base указывает на папку с шаблонным файлом
  // в имени файла (при сохранении через gulp.dest) не будет имен папок
  newFile.path = newPath;
  newFile.base = dirname;
  newFile.data = data;

  newFile.source = templateFile.path;

  // mtime ставим по максимальному из переданных
  newFile.stat.mtime = maxDate(
    templateFile.stat.mtime,
    ...files.map(f => f.stat.mtime),
  );

  return newFile;
};


const sprite = (templates, name) => {
  // минимальный конфиг для SVGSpriter
  const config = {
    shape: {
      transform: [],
    },
    mode: {
      css: true,
    },
  };
  // SVGSpriter все равно делает много лишнего :(
  const spriter = new SVGSpriter(config);
  const files = [];

  const filename = ext => `${name}.${ext}`;

  return through.obj((file, encoding, callback) => {
    withValidFile(file, callback, (validFile) => {
      // запоминаем файлы
      // потом используем их mtime и прочее
      // для установки аттрибутов спрайту
      files.push(validFile);

      try {
        // добавляем в SVGSpriter
        spriter.add(validFile);
        callback();
      } catch (e) {
        callback(pluginError(e.message));
      }
    });
  }, function endStream(callback) {
    Promise.all(templates.map(t => vinylFile.read(t))).catch((err) => {
      callback(pluginError(err));
    }).then((templateFiles) => {
      spriter.compile((err, result, data) => {
        if (err) {
          callback(err);
          return;
        }

        // если нет спрайтов, то ничего не генерируем
        if (!data.css.shapes.length || !files.length) return;

        // если просто отдать готовый спрайт в поток
        // то у него будет непонятное время модификации
        // по которому не сработает gulp-newer
        // берем за основу первый файл (используеются его пути)
        // а mtime собираем из всех принятых файлов
        const resultFile = createTemplateFile(
          files[0],
          () => filename('svg'),
          files,
        );
        // заменим контент
        resultFile.contents = result.css.sprite.contents;

        // шлем спрайт в поток
        this.push(resultFile);

        // в поток попадают файлы вида car.svg и car.hover.svg
        // считаем, что файлы с точкой в имени являются модификаторами основной иконки
        // надо чтобы в глобальные переменные попадали только "обычные" файлы
        // остальные должны быть как аттрибуты файлов верхнего уровня
        // shapes = { car: { ..., hover: { ... } } }

        const shapes = {};

        data.css.shapes.forEach((s) => {
          const shapeData = {
            width: s.width.outer,
            height: s.height.outer,
            x: s.position.absolute.x,
            y: s.position.absolute.y,
            position: s.position.absolute.xy,
          };

          const [icon, additional] = splitFilename(s.name);
          if (!icon) return;

          const current = shapes[icon] || {};

          if (additional) {
            // модифицированая иконка
            // добавляем как аттрибут к основной
            shapes[icon] = {
              [additional]: shapeData,
              ...current,
            };
          } else {
            // основная иконка
            shapes[icon] = {
              ...current,
              ...shapeData,
              basename: icon,
            };
          }
        });

        templateFiles.forEach((templateFile) => {
          // загружаем файл с шаблоном и устанавливаем глобальные переменные
          this.push(createTemplateFile(
            templateFile,
            (basename, extname) => `${name}_${basename}${extname}`,
            files,
            {
              sprite: name,
              names: {
                svg: filename('svg'),
                png: filename('png'),
              },
              shapes: Object.values(shapes),
            },
          ));
        });

        callback();
      });
    });
  });
};

module.exports = { sprite };
