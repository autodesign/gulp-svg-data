'use strict';

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _through = require('through2');

var _through2 = _interopRequireDefault(_through);

var _svgSprite = require('svg-sprite');

var _svgSprite2 = _interopRequireDefault(_svgSprite);

var _gulpUtil = require('gulp-util');

var _gulpUtil2 = _interopRequireDefault(_gulpUtil);

var _vinylFile = require('vinyl-file');

var _vinylFile2 = _interopRequireDefault(_vinylFile);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pluginError = function pluginError(t) {
  return new _gulpUtil2.default.PluginError('gulp-svg-data', t);
};

var maxDate = function maxDate() {
  return new Date(Math.max.apply(Math, arguments));
};

var splitFilename = function splitFilename(f) {
  var parts = f.split('.');

  return parts.length === 1 ? parts : [parts.slice(0, -1).join('.'), parts.slice(-1)];
};

var withValidFile = function withValidFile(file, cb, then) {
  if (file.isNull()) {
    cb();
    return;
  }

  if (file.isStream()) {
    cb(pluginError('Streaming not supported'));
    return;
  }

  then(file);
};

var createTemplateFile = function createTemplateFile(templateFile, createName, files) {
  var data = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

  var dirname = _path2.default.dirname(templateFile.path);
  var extname = _path2.default.extname(templateFile.path);
  var basename = _path2.default.basename(templateFile.path, extname);

  var newPath = _path2.default.join(dirname, createName(basename, extname));

  var newFile = templateFile.clone();

  newFile.path = newPath;
  newFile.base = dirname;
  newFile.data = data;

  newFile.source = templateFile.path;

  newFile.stat.mtime = maxDate.apply(undefined, [templateFile.stat.mtime].concat((0, _toConsumableArray3.default)(files.map(function (f) {
    return f.stat.mtime;
  }))));

  return newFile;
};

var sprite = function sprite(templates, name) {
  var config = {
    shape: {
      transform: []
    },
    mode: {
      css: true
    }
  };

  var spriter = new _svgSprite2.default(config);
  var files = [];

  var filename = function filename(ext) {
    return `${ name }.${ ext }`;
  };

  return _through2.default.obj(function (file, encoding, callback) {
    withValidFile(file, callback, function (validFile) {
      files.push(validFile);

      try {
        spriter.add(validFile);
        callback();
      } catch (e) {
        callback(pluginError(e.message));
      }
    });
  }, function endStream(callback) {
    var _this = this;

    _promise2.default.all(templates.map(function (t) {
      return _vinylFile2.default.read(t);
    })).catch(function (err) {
      callback(pluginError(err));
    }).then(function (templateFiles) {
      spriter.compile(function (err, result, data) {
        if (err) {
          callback(err);
          return;
        }

        if (!data.css.shapes.length || !files.length) return;

        var resultFile = createTemplateFile(files[0], function () {
          return filename('svg');
        }, files);

        resultFile.contents = result.css.sprite.contents;

        _this.push(resultFile);

        var shapes = {};

        data.css.shapes.forEach(function (s) {
          var shapeData = {
            width: s.width.outer,
            height: s.height.outer,
            x: s.position.absolute.x,
            y: s.position.absolute.y,
            position: s.position.absolute.xy
          };

          var _splitFilename = splitFilename(s.name),
              _splitFilename2 = (0, _slicedToArray3.default)(_splitFilename, 2),
              icon = _splitFilename2[0],
              additional = _splitFilename2[1];

          if (!icon) return;

          var current = shapes[icon] || {};

          if (additional) {
            shapes[icon] = (0, _extends3.default)({
              [additional]: shapeData
            }, current);
          } else {
            shapes[icon] = (0, _extends3.default)({}, current, shapeData, {
              basename: icon
            });
          }
        });

        templateFiles.forEach(function (templateFile) {
          _this.push(createTemplateFile(templateFile, function (basename, extname) {
            return `${ name }_${ basename }${ extname }`;
          }, files, {
            sprite: name,
            names: {
              svg: filename('svg'),
              png: filename('png')
            },
            shapes: (0, _values2.default)(shapes)
          }));
        });

        callback();
      });
    });
  });
};

module.exports = { sprite };
